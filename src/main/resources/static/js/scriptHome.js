$(document).ready(function() {

    bsCustomFileInput.init();

    $("#mainContent").children().not(":first").css("display", "none");
    $("#customFile4").parent().css("display", "none");
    $("#customFile5").parent().css("display", "none");

    $("#dashboardLinks li a").click(function (e) {

        e.preventDefault();

        var lastChildEffect = 0;

        if(!$(this).hasClass("active")) {

            $("#mainContent").children().hide(function (){

                lastChildEffect++;

                if(lastChildEffect == $("#mainContent").children().length) {

                    if ($(e.target).attr("href") == "employeesTable") {

                        $("#mainContent").addClass("table-padding");

                    } else {

                        $("#mainContent").removeClass("table-padding");

                    }

                    $("#" + $(e.target).attr("href")).show();

                }

            });

        }

        $("#dashboardLinks li a").toggleClass("active", false);
        $(this).toggleClass("active");

    })

    $("#sacCheckbox").change(function() {

        $("#customFile4").parent().toggle(this.checked);
        $("#customFile5").parent().toggle(this.checked);

    })

    $("#fileSubmit").click(function (e) {

        e.preventDefault();

        var file1 = document.getElementById('customFile1').files[0];
        var file2 = document.getElementById('customFile2').files[0];
        var file3 = document.getElementById('customFile3').files[0];
        var file4 = document.getElementById('customFile4').files[0];
        var file5 = document.getElementById('customFile5').files[0];

        var formData = new FormData();
        formData.append('file', file1);
        formData.append('file', file2);
        formData.append('file', file3);
        formData.append('file', file4);
        formData.append('file', file5);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "http://localhost:8080/api/files",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 100000,
            success: function (data) {

                console.log("SUCCESS", data);

            },
            error: function (e) {

                $("#result").text(e.responseText);
                console.log("ERROR : ", e);

            }
        })

    })

    $("#cuilListSubmit").click(function (e) {

        e.preventDefault();

        cuils = $("#cuilArea").val().split('\n');

        $.ajax({
            type: "POST",
            contentType : "application/json",
            url: "http://localhost:8080/api/cuils",
            data: JSON.stringify({cuils:cuils}),
            dataType: "json",
            timeout: 100000,
            success: function (data) {

                var cont = 0;

                $.get(location.href + "api/findValues/", function() {

                    console.log("Success")

                })

                $("#tableBody").empty();

                $.each(data, function(key, value) {

                    $("#tableBody").append('<tr id="' + value + '">'
                        + '<td class="btn-group">'
                        + '<button class="btn btn-outline-danger btn-sm" type="button">'
                        + '<span class="oi oi-trash"></span>'
                        + '</button>'
                        + '<button class="btn btn-outline-info btn-sm" type="button">'
                        + '<span class="oi oi-loop-circular"></span>'
                        + '</button>'
                        + '</td>'
                        + '<td scope="row">' + value + '</td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                        + '</tr>');

                });

                $("#tableBody").append('<tr>'
                    +	'<td></td>'
                    +	'<td style="min-width: 150px;">'
                    +		'<input type="text" class="form-control" placeholder="CUIL" id="newCuil">'
                    +	'</td>'
                    + '</tr>');

            },
            error: function (e) {

                $("#result").text(e.responseText);
                console.log("ERROR : ", e);

            }

        })

    })

    $("#loadAll").on('click', function(){

        var inputArray;

        $.get(location.href + "api/findValues", function(response) {

            $.each(response.data, function (cuil, valueArray) {

                inputArray = $("#" + cuil + " > td > input");

                $.each(valueArray, function (index, value) {

                    inputArray[index].value = value;

                })

            })

        })

    })

    $("#tableBody").on('click','.btn-outline-danger',function(){

        $(this).parent().parent().remove();

    })

    $("#tableBody").on('focusout', '#newCuil', function(){

        if ($(this).val() != '') {

            $(this).parent().parent().before('<tr id="' + $(this).val() + '">'
                +	'<td class="btn-group">'
                +		'<button class="btn btn-outline-danger btn-sm" type="button">'
                +			'<span class="oi oi-trash"></span>'
                +		'</button>'
                +		'<button class="btn btn-outline-info btn-sm" type="button">'
                +			'<span class="oi oi-loop-circular"></span>'
                +		'</button>'
                +	'</td>'
                +	'<td scope="row">' + $(this).val() + '</td>'
                +	'<td style="min-width: 300px;"><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                +	'<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>' + '<td><input type="text" class="form-control"></td>'
                + '</tr>')

            $(this).val('');

        }

    })

    $("#tableBody").on('click','.btn-outline-info',function(e) {

        var cuil = $(this).parent().parent().attr("id");
        var inputArray = $("#" + cuil + " > td > input");

        $.get(location.href + "api/findValues/" + cuil, function(response) {

            $.each(response.data, function (index, value) {

                inputArray[index].value = value;

            })

        })

    })

    $("#download").on('click', function(){

        textFileGenerator();

    });

    function textFileGenerator(){

        var allRows = $("#tableBody").children();

        var outputString = "";

        for (var i = 0; i < allRows.length - 1; i++) {

            var workString;

            //CUIL
            workString = allRows.eq(i).children().eq(1).text();

            //Apellido y Nombre
            workString += allRows.eq(i).children().eq(2).children().first().val().padEnd(30);

            //Cónyuge
            workString += (allRows.eq(i).children().eq(3).children().first().val() == 'Si') ? '1' : '0';

            //Cantidad de hijos
            workString += allRows.eq(i).children().eq(4).children().first().val().padStart(2,'0');

            //Código de situación
            workString += allRows.eq(i).children().eq(5).children().first().val().padStart(2,'0');

            //Código de condición
            workString += allRows.eq(i).children().eq(6).children().first().val().padStart(2,'0');

            //Código de actividad
            workString += allRows.eq(i).children().eq(7).children().first().val().padStart(3,'0');

            //Código de zona
            workString += allRows.eq(i).children().eq(8).children().first().val().padStart(2,'0');

            //Porcentaje de aporte adicional SS
            workString += parseFloat(allRows.eq(i).children().eq(9).children().first().val()).toFixed(2).padStart(5,'0');

            //Código de modalidad de contratación
            workString += allRows.eq(i).children().eq(10).children().first().val().padStart(3,'0');

            //Código de obra social
            workString += allRows.eq(i).children().eq(11).children().first().val().padStart(6,'0');

            //Cantidad de adherentes
            workString += allRows.eq(i).children().eq(12).children().first().val().padStart(2,'0');

            //Remuneración total
            workString += parseFloat(allRows.eq(i).children().eq(13).children().first().val()).toFixed(2).padStart(12,'0');

            //Remuneración imponible 1
            workString += parseFloat(allRows.eq(i).children().eq(14).children().first().val()).toFixed(2).padStart(12,'0');

            //Asignaciones familiares pagadas
            workString += parseFloat(allRows.eq(i).children().eq(15).children().first().val()).toFixed(2).padStart(9,'0');

            //Importe aporte voluntario
            workString += parseFloat(allRows.eq(i).children().eq(16).children().first().val()).toFixed(2).padStart(9,'0');

            //Importe adicional OS
            workString += parseFloat(allRows.eq(i).children().eq(17).children().first().val()).toFixed(2).padStart(9,'0');

            //Importe excedente aportes SS
            workString += parseFloat(allRows.eq(i).children().eq(18).children().first().val()).toFixed(2).padStart(9,'0');

            //Importe excedente aportes OS
            workString += parseFloat(allRows.eq(i).children().eq(19).children().first().val()).toFixed(2).padStart(9,'0');

            //Provincia localidad
            workString += allRows.eq(i).children().eq(20).children().first().val().padEnd(50);

            //Remuneración imponible 2
            workString += parseFloat(allRows.eq(i).children().eq(21).children().first().val()).toFixed(2).padStart(12,'0');

            //Remuneración imponible 3
            workString += parseFloat(allRows.eq(i).children().eq(22).children().first().val()).toFixed(2).padStart(12,'0');

            //Remuneración imponible 4
            workString += parseFloat(allRows.eq(i).children().eq(23).children().first().val()).toFixed(2).padStart(12,'0');

            //Código de siniestrado
            workString += allRows.eq(i).children().eq(24).children().first().val().padStart(2,'0');

            //Marca de corresponde reducción
            workString += (allRows.eq(i).children().eq(25).children().first().val() == 'Si') ? '1' : '0';

            //Capital de recomposición de LRT
            workString += parseFloat(allRows.eq(i).children().eq(26).children().first().val()).toFixed(2).padStart(9,'0');

            //Tipo de empresa
            workString += allRows.eq(i).children().eq(27).children().first().val().padStart(1,'0');

            //Aporte adicional de obra social
            workString += parseFloat(allRows.eq(i).children().eq(28).children().first().val()).toFixed(2).padStart(9,'0');

            //Régimen
            workString += allRows.eq(i).children().eq(29).children().first().val().padStart(1,'0');

            //Situación de revista 1
            workString += allRows.eq(i).children().eq(30).children().first().val().padStart(2,'0');

            //Día de inicio situación de revista 1
            workString += allRows.eq(i).children().eq(31).children().first().val().padStart(2,'0');

            //Situación de revista 2
            workString += allRows.eq(i).children().eq(32).children().first().val().padStart(2,'0');

            //Día de inicio situación de revista 2
            workString += allRows.eq(i).children().eq(33).children().first().val().padStart(2,'0');

            //Situación de revista 3
            workString += allRows.eq(i).children().eq(34).children().first().val().padStart(2,'0');

            //Día de inicio situación de revista 3
            workString += allRows.eq(i).children().eq(35).children().first().val().padStart(2,'0');

            //Sueldo + adicionales
            workString += parseFloat(allRows.eq(i).children().eq(36).children().first().val()).toFixed(2).padStart(12,'0');

            //SAC
            workString += parseFloat(allRows.eq(i).children().eq(37).children().first().val()).toFixed(2).padStart(12,'0');

            //Horas extras
            workString += parseFloat(allRows.eq(i).children().eq(38).children().first().val()).toFixed(2).padStart(12,'0');

            //Zona desfavorable
            workString += parseFloat(allRows.eq(i).children().eq(39).children().first().val()).toFixed(2).padStart(12,'0');

            //Vacaciones
            workString += parseFloat(allRows.eq(i).children().eq(40).children().first().val()).toFixed(2).padStart(12,'0');

            //Cantidad de días trabajados
            workString += parseFloat(allRows.eq(i).children().eq(41).children().first().val()).toFixed(2).padStart(9,'0');

            //Remuneración imponible 5
            workString += parseFloat(allRows.eq(i).children().eq(42).children().first().val()).toFixed(2).padStart(12,'0');

            //Trabajador convencionado (0-No 1-Si)
            workString += allRows.eq(i).children().eq(43).children().first().val().padStart(1,'0');

            //Remuneración imponible 6
            workString += parseFloat(allRows.eq(i).children().eq(44).children().first().val()).toFixed(2).padStart(12,'0');

            //Tipo de operación
            workString += allRows.eq(i).children().eq(45).children().first().val().padStart(1,'0');

            //Adicionales
            workString += parseFloat(allRows.eq(i).children().eq(46).children().first().val()).toFixed(2).padStart(12,'0');

            //Premios
            workString += parseFloat(allRows.eq(i).children().eq(47).children().first().val()).toFixed(2).padStart(12,'0');

            //Rem. Dto. 788/05 / Remuneración 8
            workString += parseFloat(allRows.eq(i).children().eq(48).children().first().val()).toFixed(2).padStart(12,'0');

            //Remuneración imponible 7
            workString += parseFloat(allRows.eq(i).children().eq(49).children().first().val()).toFixed(2).padStart(12,'0');

            //Cantidad de horas extras
            workString += allRows.eq(i).children().eq(50).children().first().val().padStart(3,'0');

            //Conceptos No remunerativos
            workString += parseFloat(allRows.eq(i).children().eq(51).children().first().val()).toFixed(2).padStart(12,'0');

            //Maternidad
            workString += parseFloat(allRows.eq(i).children().eq(52).children().first().val()).toFixed(2).padStart(12,'0');

            //Rectificación de Remuneración
            workString += parseFloat(allRows.eq(i).children().eq(53).children().first().val()).toFixed(2).padStart(9,'0');

            //Remuneración Imponible 9
            workString += parseFloat(allRows.eq(i).children().eq(54).children().first().val()).toFixed(2).padStart(12,'0');

            //Contribución Tarea Diferencial %
            workString += parseFloat(allRows.eq(i).children().eq(55).children().first().val()).toFixed(2).padStart(9,'0');

            //Horas Trabajadas
            workString += parseFloat(allRows.eq(i).children().eq(56).children().first().val()).toFixed().padStart(3,'0');

            //Seguro Colectivo de Vida Obligatorio
            workString += allRows.eq(i).children().eq(57).children().first().val().padStart(1,'0');

            //Importe detracción Ley 27430
            workString += parseFloat(allRows.eq(i).children().eq(58).children().first().val()).toFixed(2).padStart(12,'0');

            //Incremento salarial
            workString += parseFloat(allRows.eq(i).children().eq(59).children().first().val()).toFixed(2).padStart(12,'0');

            //Remuneración Imponible 11
            workString += parseFloat(allRows.eq(i).children().eq(60).children().first().val()).toFixed(2).padStart(12,'0');

            if (i != allRows.length - 2) {

                //Salto de Linea
                workString += "\n";

            }

            workString = workString.replace(/\./g, ',');

            outputString += workString;

        }

        var blob = new Blob([outputString], {type: 'text/plain;charset=windows-1252;'});
        saveAs(blob, 'AFIP.txt');

    }

})