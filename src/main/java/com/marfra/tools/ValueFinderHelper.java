package com.marfra.tools;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@Service
public class ValueFinderHelper {

    public String findValue(Map<Integer, List<String>>[] files, String cuil, Integer cuilIdx, String[] valueColNames, Integer colNamesIdx) {

        String result = "";
        int cont;

        for (Map<Integer, List<String>> file : files) {

            List<String> employeeData = new ArrayList<>();
            List<String> colNames = file.get(colNamesIdx);
            cont = 3;

            for (Map.Entry<Integer, List<String>> entry : file.entrySet()) {

                if (entry.getValue().get(cuilIdx).equals(cuil)) {

                    employeeData = entry.getValue();
                    break;

                }

            }

            if (employeeData.isEmpty()) {

                //Add to employeeData.xls
                //Default value?
                break;

            }

            for (String valueColName : valueColNames) {

                for (String colName : colNames) {

                    if (colName.contains(valueColName)) {

                        if (valueColName.equals("Unid./Porc.") && employeeData.get(cont).equals("")) {

                            if (cont == 7) {

                                result = "0";
                                break;

                            }

                            cont = cont + 2;
                            continue;

                        }

                        if (valueColName.equals("Unid./Porc.") && !employeeData.get(cont).equals("")) {

                            if (valueColNames[0].equals(valueColName) && files[0].equals(file)) {

                                result = employeeData.get(cont).replace(',', '.');

                            } else {

                                if (result.equals("")) {

                                    result = "0";

                                }

                                if (employeeData.get(cont).equals("")) {

                                    result = String.valueOf(Double.parseDouble(result.replace(',', '.')) + Double.parseDouble("0"));

                                } else {

                                    result = String.valueOf(Double.parseDouble(result.replace(',', '.')) + Double.parseDouble(employeeData.get(cont).replace(',', '.')));

                                }

                            }

                            break;

                        }

                        if (valueColNames[0].equals(valueColName) && files[0].equals(file)) {

                            result = employeeData.get(colNames.indexOf(colName)).replace(',', '.');

                        } else {

                            if (result.equals("")) {

                                result = "0";

                            }

                            if (employeeData.get(colNames.indexOf(colName)).equals("")) {

                                result = String.valueOf(Double.parseDouble(result.replace(',', '.')) + Double.parseDouble("0"));

                            } else {

                                result = String.valueOf(Double.parseDouble(result.replace(',', '.')) + Double.parseDouble(employeeData.get(colNames.indexOf(colName)).replace(',', '.')));

                            }

                        }

                        break;

                    }

                }

            }

        }

        return result;

    }

}
