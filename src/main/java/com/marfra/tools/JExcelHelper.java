package com.marfra.tools;

import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import jxl.write.Number;
import jxl.write.*;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class JExcelHelper {

    public Map<Integer, List<String>> readJExcel(InputStream inputStream)
            throws IOException, BiffException {

        Map<Integer, List<String>> data = new HashMap<>();

        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("Cp1252");

        Workbook workbook = Workbook.getWorkbook (inputStream,ws);
        Sheet sheet = workbook.getSheet(0);
        int rows = sheet.getRows();
        int columns = sheet.getColumns();

        for (int i = 0; i < rows; i++) {
            data.put(i, new ArrayList<String>());
            for (int j = 0; j < columns; j++) {
                data.get(i)
                        .add(sheet.getCell(j, i)
                                .getContents());
            }
        }
        return data;
    }

    public void writeJExcel() throws IOException, WriteException, BiffException {
        WritableWorkbook workbook = null;
        try {
            File currDir = new File("temp.xls");
            String path = currDir.getAbsolutePath();
            System.out.println(path);
            Workbook workbookPemanent = Workbook.getWorkbook(currDir);

            workbook = Workbook.createWorkbook(new File(path),workbookPemanent);

            //WritableSheet sheet = workbook.createSheet("Sheet 1", 0);
            WritableSheet sheet = workbook.getSheet(0);

            /*
            Label headerLabel = new Label(0, 0, "Name");
            sheet.setColumnView(0, 60);
            sheet.addCell(headerLabel);

            headerLabel = new Label(1, 0, "Age");
            sheet.setColumnView(0, 40);
            sheet.addCell(headerLabel);
             */

            Label cellLabel = new Label(0, 2, "John Rambo");
            sheet.addCell(cellLabel);
            Number cellNumber = new Number(1, 2, 20);
            sheet.addCell(cellNumber);

            workbook.write();
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }

    }

}
