package com.marfra.controllers;

import com.marfra.tools.JExcelHelper;
import com.marfra.tools.ValueFinderHelper;
import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FormManagerRestController {

    @Autowired
    private JExcelHelper jExcelHelper;

    @Autowired
    private ValueFinderHelper valueFinderHelper;

    private Map<Integer, List<String>>[] allFiles;
    private String[] cuilList;
    private Map<String, String[]> allEmployeeInfo;

    @PostMapping(value="/api/files")
    public Map<Integer, List<String>>[] uploadFile(@RequestBody MultipartFile[] file) throws IOException, BiffException, WriteException {

        allFiles = new Map[6];

        File employeesData = new File("employeesData.xls");
        InputStream employeesStream =  new FileInputStream(employeesData);
        allFiles[0] = jExcelHelper.readJExcel(employeesStream);

        for (int i = 0; i < file.length; i++) {

            MultipartFile f = file[i];

            InputStream inputStream = f.getInputStream();

            allFiles[i+1] = jExcelHelper.readJExcel(inputStream);

        }

        return allFiles;

    }

    @GetMapping(value="/api/files")
    public Map showFile() {

        Map<String, Object> result = new HashMap<>();

        result.put("timestamp", System.currentTimeMillis());
        result.put("status", allFiles.length > 0 ? 200 : 404);
        result.put("message", allFiles.length > 0 ? "Data found" : "Empty response");
        result.put("data", allFiles);

        return result;

    }

    @GetMapping(value = "/api/files/{id}")
    public Map showFileById(@PathVariable("id") int id) {

        Map<String, Object> result = new HashMap<>();

        result.put("timestamp", System.currentTimeMillis());
        result.put("status", allFiles.length > 0 ? 200 : 404);
        result.put("message", allFiles.length > 0 ? "Data found" : "Empty response");
        result.put("data", allFiles[id]);

        return result;

    }

    @GetMapping(value="/api/findValues")
    public Map findValues() {

        allEmployeeInfo = new HashMap<>();

        for (String cuil : cuilList) {

            String[] employeeInfo = new String[59];
            boolean monthly = false;

            for (Map.Entry<Integer, List<String>> entry : allFiles[1].entrySet()) {

                if (entry.getKey() > 1) {

                    if (entry.getValue().get(2).equals(cuil)) {

                        monthly = true;
                        break;

                    }

                }

            }

            //Apellido y Nombre
            employeeInfo[0] = valueFinderHelper.findValue(new Map[]{allFiles[0]},cuil,0, new String[]{"Apellido y Nombre"},0);

            //Cónyuge
            employeeInfo[1] = valueFinderHelper.findValue(new Map[]{allFiles[0]},cuil,0, new String[]{"Cónyuge"},0);

            if (employeeInfo[1].equals("")) {

                //Default
                employeeInfo[1] = "0";

            }

            //Cantidad de hijos
            employeeInfo[2] = valueFinderHelper.findValue(new Map[]{allFiles[0]},cuil,0, new String[]{"Cantidad de hijos"},0);

            if (employeeInfo[2].equals("")) {

                //Default
                employeeInfo[2] = "0";

            }

            //Código de Condición
            employeeInfo[4] = "1";

            //Código de Actividad
            employeeInfo[5] = valueFinderHelper.findValue(new Map[]{allFiles[0]},cuil,0, new String[]{"Código de Actividad"},0);

            //Código de Zona
            employeeInfo[6] = "27";

            //Porcentaje de aporte adicional SS
            employeeInfo[7] = "0";

            //Código de modalidad de contratación
            employeeInfo[8] = valueFinderHelper.findValue(new Map[]{allFiles[0]},cuil,0, new String[]{"Código de modalidad de contratación"},0);

            //Código de obra social
            employeeInfo[9] = valueFinderHelper.findValue(new Map[]{allFiles[0]},cuil,0, new String[]{"Código de obra social"},0);

            //Cantidad de adherentes
            employeeInfo[10] = "0";

            //Asignaciones familiares pagadas
            employeeInfo[13] = "0";

            //Importe aporte voluntario
            employeeInfo[14] = "0";

            //Importe adicional OS
            employeeInfo[15] = "0";

            //Importe excedente aportes SS
            employeeInfo[16] = "0";

            //Excedentes Aportes SS
            employeeInfo[17] = "0";

            //Provincia localidad
            employeeInfo[18] = valueFinderHelper.findValue(new Map[]{allFiles[0]},cuil,0, new String[]{"Provincia localidad"},0);

            //Código de siniestrado
            //Default value - Manually change
            //Check concept "0020 Hs. Acc.Trabajo"
            employeeInfo[22] = "0";

            //Marca de corresponde reducción
            employeeInfo[23] = "0";

            //Capital de recomposición de LRT
            employeeInfo[24] = "0";

            //Tipo de empresa
            employeeInfo[25] = "1";

            //Aporte adicional de obra social
            employeeInfo[26] = "0";

            //Régimen
            employeeInfo[27] = "1";

            //Sueldo + adicionales (Only Sueldo, bad concept name)
            if (monthly) {

                if(cuil.equals("20133500163")) {

                    employeeInfo[34] = valueFinderHelper.findValue(new Map[]{allFiles[1]},cuil,2, new String[]{"0001 Horas Simples"},1);;

                } else {

                    employeeInfo[34] = valueFinderHelper.findValue(new Map[]{allFiles[1]},cuil,2, new String[]{"0101 Sueldo Básico"},1);

                }

            } else {

                employeeInfo[34] = valueFinderHelper.findValue(new Map[]{allFiles[2],allFiles[3]},cuil,2, new String[]{"0001 Horas Simples","0002 Horas Simples (2da)","0003 Horas Simples (Esp)","0018 Hs. Enfermedad ","0020 Hs. Acc.Trabajo"},1);

            }

            if (employeeInfo[34].equals("")) {

                //Default
                employeeInfo[34] = "0";

            }

            //SAC
            //AKA Aguinaldo - File not always present
            //different sac file vs cese/fortnight file value (probably not possible to be both at the same time)
            if (monthly) {

                //File SAC
                if (allFiles[4] == null) {

                    employeeInfo[35] = "0";

                } else {

                    employeeInfo[35] = valueFinderHelper.findValue(new Map[]{allFiles[4]},cuil,2, new String[]{"0113 SAC 1er. Sem/20"},1);

                }

                String innerSAC = valueFinderHelper.findValue(new Map[]{allFiles[1]},cuil,2, new String[]{"0113 SAC"},1);

                if (!innerSAC.equals("")) {

                    employeeInfo[35] = String.valueOf(Double.parseDouble(employeeInfo[35]) + Double.parseDouble(innerSAC));

                }


            } else {

                //File SAC
                if (allFiles[5] == null) {

                    employeeInfo[35] = "0";

                } else {

                    employeeInfo[35] = valueFinderHelper.findValue(new Map[]{allFiles[5]},cuil,2, new String[]{"0113 SAC 1er. Sem/20"},1);

                }

                String innerSAC = valueFinderHelper.findValue(new Map[]{allFiles[2],allFiles[3]},cuil,2, new String[]{"0013 SAC"},1);

                if (!innerSAC.equals("")) {

                    employeeInfo[35] = String.valueOf(Double.parseDouble(employeeInfo[35]) + Double.parseDouble(innerSAC));

                }

            }

            if (employeeInfo[35].equals("")) {

                //Default
                employeeInfo[35] = "0";

            }

            //Horas extras
            employeeInfo[36] = "0";

            //Zona desfavorable
            employeeInfo[37] = "0";

            //Vacaciones
            if (monthly) {

                employeeInfo[38] = valueFinderHelper.findValue(new Map[]{allFiles[1]},cuil,2, new String[]{"0112 Vacaciones Anuales"},1);

            } else {

                employeeInfo[38] = valueFinderHelper.findValue(new Map[]{allFiles[2],allFiles[3]},cuil,2, new String[]{"0012 Vacac. Anuales"},1);

            }

            if (employeeInfo[38].equals("")) {

                //Default
                employeeInfo[38] = "0";

            }

            //Cantidad de días trabajados
            if (monthly) {

                if(cuil.equals("20133500163")) {

                    employeeInfo[39] = "0";

                } else {

                    employeeInfo[39] = "30";

                }

            } else {

                employeeInfo[39] = "0";

            }

            //Trabajador convencionado (0-No 1-Si)
            employeeInfo[41] = valueFinderHelper.findValue(new Map[]{allFiles[0]},cuil,0, new String[]{"Trabajador convencionado (0-No 1-Si)"},0);

            if (employeeInfo[41].equals("")) {

                //Default
                employeeInfo[41] = "1";

            }

            //Remuneración imponible 6
            employeeInfo[42] = "0";

            //Tipo de operación
            employeeInfo[43] = "0";

            //Adicionales
            //Feriado + Antiguedad
            if (monthly) {

                if(cuil.equals("20133500163")) {

                    employeeInfo[44] = valueFinderHelper.findValue(new Map[]{allFiles[1]},cuil,2, new String[]{"0005 Feriado Nacional","0041 Antigüedad"},1);


                } else {

                    employeeInfo[44] = valueFinderHelper.findValue(new Map[]{allFiles[1]},cuil,2, new String[]{"0106 Plus Feriado Nac.","0140 Antigüedad","0141 Antigüedad","0142 Antigüedad"},1);

                }

            } else {

                employeeInfo[44] = valueFinderHelper.findValue(new Map[]{allFiles[2],allFiles[3]},cuil,2, new String[]{"0005 Feriado Nacional","0041 Antigüedad"},1);

            }

            if (employeeInfo[44].equals("")) {

                //Default
                employeeInfo[44] = "0";

            }

            //Premios
            //Asistencia + Productividad
            if (monthly) {

                if (cuil.equals("20133500163")) {

                    employeeInfo[45] = valueFinderHelper.findValue(new Map[]{allFiles[1]},cuil,2, new String[]{"0007 Asistencia","0004 Productividad "},1);

                } else {

                    employeeInfo[45] = valueFinderHelper.findValue(new Map[]{allFiles[1]},cuil,2, new String[]{"0124 Premio por Asistencia","0125 Premio por Productividad"},1);

                }

            } else {

                employeeInfo[45] = valueFinderHelper.findValue(new Map[]{allFiles[2],allFiles[3]},cuil,2, new String[]{"0007 Asistencia","0004 Productividad "},1);

            }

            if (employeeInfo[45].equals("")) {

                //Default
                employeeInfo[45] = "0";

            }

            //Remuneración imponible 7
            employeeInfo[47] = "0";

            //Cantidad de horas extras
            employeeInfo[48] = "0";

            //Conceptos No remunerativos
            employeeInfo[49] = "0";

            //Maternidad
            employeeInfo[50] = "0";

            //Rectificación de Remuneración
            employeeInfo[51] = "0";

            //Contribución Tarea Diferencial %
            employeeInfo[53] = "0";

            //Horas Trabajadas
            //Not injured nor illness hours
            if (monthly) {

                if(cuil.equals("20133500163")) {

                    employeeInfo[54] = "200";

                } else {

                    employeeInfo[54] = "0";

                }

            } else {

                employeeInfo[54] = valueFinderHelper.findValue(new Map[]{allFiles[2],allFiles[3]},cuil,2, new String[]{"Unid./Porc."},1);

            }

            if (employeeInfo[54].equals("")) {

                //Default
                employeeInfo[54] = "0";

            }

            //Seguro Colectivo de Vida Obligatorio
            employeeInfo[55] = "1";

            //Incremento salarial
            //Create logic for variable raise
            /* int salaryRaise = 20;
            if (monthly) {

                employeeInfo[57] = "4000";

            } else {

                //Check what hours count
                employeeInfo[57] = String.valueOf(Double.parseDouble(employeeInfo[54].replace(',', '.')) * salaryRaise);

            }
            */

            employeeInfo[57] = "0";


            //Remuneración Imponible 11
            employeeInfo[58] = "0";

            //--------------------//
            // Dependant Concepts //
            //--------------------//

            //Remuneración total
            //sueldo + adicionales + horas extras + plus por zona desfavorable + SAC + vacaciones + premios + incremento salarial

            employeeInfo[11] = String.valueOf(Double.parseDouble(employeeInfo[34]) + Double.parseDouble(employeeInfo[44])
                                                + Double.parseDouble(employeeInfo[35]) + Double.parseDouble(employeeInfo[38])
                                                + Double.parseDouble(employeeInfo[45]) + Double.parseDouble(employeeInfo[57]));

            if (employeeInfo[11].equals("")) {

                //Default
                employeeInfo[11] = "0";

            }

            //Remuneración imponible 1
            employeeInfo[12] = employeeInfo[11];

            //Remuneración imponible 2
            employeeInfo[19] = employeeInfo[11];

            //Remuneración imponible 3
            employeeInfo[20] = employeeInfo[11];

            //Remuneración imponible 4
            employeeInfo[21] = employeeInfo[11];

            //Remuneración imponible 5
            employeeInfo[40] = employeeInfo[11];

            //Rem. Dto. 788/05 / Remuneración 8
            employeeInfo[46] = employeeInfo[11];

            //Remuneración Imponible 9
            employeeInfo[52] = employeeInfo[11];

            //Situación de revista 1
            if (monthly) {

                if (employeeInfo[34].equals("0") || employeeInfo[34].equals("0.0")) {

                    employeeInfo[28] = "21";

                } else {

                    //Lacking if contains SAC in salary file (name not known)
                    employeeInfo[28] = "1";

                }

            } else {

                if (employeeInfo[34].equals("0") || employeeInfo[34].equals("0.0")) {

                    employeeInfo[28] = "21";

                } else {

                    String firstFortnightSalary = valueFinderHelper.findValue(new Map[]{allFiles[2]},cuil,2, new String[]{"0001 Horas Simples","0002 Horas Simples (2da)","0003 Horas Simples (Esp)","0018 Hs. Enfermedad ","0020 Hs. Acc.Trabajo"},1);
                    if (firstFortnightSalary.equals("0") || firstFortnightSalary.equals("0.0") || firstFortnightSalary.equals("")) {

                        employeeInfo[28] = "21";

                    } else {

                        if (valueFinderHelper.findValue(new Map[]{allFiles[2]},cuil,2, new String[]{"0013 SAC"},1).equals("")) {

                            employeeInfo[28] = "1";

                        } else {

                            employeeInfo[28] = "21";

                        }

                    }

                }

            }

            //Día de inicio situación de revista 1
            employeeInfo[29] = "1";

            //Situación de revista 2
            if (monthly) {

                employeeInfo[30] = "1";

            } else {

                if (employeeInfo[34].equals("0") || employeeInfo[34].equals("0.0")) {

                    employeeInfo[30] = "21";

                } else {

                    String secondFortnightSalary = valueFinderHelper.findValue(new Map[]{allFiles[3]},cuil,2, new String[]{"0001 Horas Simples","0002 Horas Simples (2da)","0003 Horas Simples (Esp)","0018 Hs. Enfermedad ","0020 Hs. Acc.Trabajo"},1);
                    if (secondFortnightSalary.equals("0") || secondFortnightSalary.equals("0.0") || secondFortnightSalary.equals("")) {

                        employeeInfo[30] = "21";

                    } else {

                        if (valueFinderHelper.findValue(new Map[]{allFiles[3]},cuil,2, new String[]{"0013 SAC"},1).equals("")) {

                            employeeInfo[30] = "1";

                        } else {

                            employeeInfo[30] = "21";

                        }

                    }

                }

            }

            //Día de inicio situación de revista 2
            if (monthly) {

                employeeInfo[31] = "0";

            } else {

                if (employeeInfo[28].equals(employeeInfo[30])) {

                    employeeInfo[31] = "0";

                } else {

                    employeeInfo[31] = "16";

                }

            }

            //Situación de revista 3
            employeeInfo[32] = "1";

            //Día de inicio situación de revista 3
            employeeInfo[33] = "0";

            //Código de Situación
            //Always last "Situación de Revista"
            employeeInfo[3] = employeeInfo[30];

            //Importe detracción Ley 27430
            double maxDetrac = 7003.68;
            double minDetrac = 5679.80;
            if (Double.parseDouble(employeeInfo[11]) < maxDetrac) {

                if (Double.parseDouble(employeeInfo[11]) - minDetrac > 0) {

                    employeeInfo[56] = String.valueOf(Double.parseDouble(employeeInfo[11]) - minDetrac);

                } else {

                    employeeInfo[56] = "0";

                }

            } else {

                employeeInfo[56] = String.valueOf(maxDetrac);

            }

            allEmployeeInfo.put(cuil, employeeInfo);

        }

        Map<String, Object> result = new HashMap<>();

        result.put("timestamp", System.currentTimeMillis());
        result.put("status", result.size() > 0 ? 200 : 404);
        result.put("message", result.size() > 0 ? "Data found" : "Empty response");
        result.put("data", allEmployeeInfo);

        return result;

    }

    @GetMapping(value = "/api/findValues/{cuil}")
    public Map findValueByCuil(@PathVariable("cuil") String cuil) {

        Map<String, Object> result = new HashMap<>();

        result.put("timestamp", System.currentTimeMillis());
        result.put("status", allEmployeeInfo.size() > 0 ? 200 : 404);
        result.put("message", allEmployeeInfo.size() > 0 ? "Data found" : "Empty response");
        result.put("data", allEmployeeInfo.get(cuil));

        return result;

    }

    @PostMapping(value="/api/cuils")
    public String[] setCuils(@RequestBody Map<String,String[]> cuils) {

        cuilList = cuils.get("cuils");

        return cuilList;

    }

    @GetMapping(value="/api/cuils")
    public Map getCuils() {

        Map<String, Object> result = new HashMap<>();

        result.put("timestamp", System.currentTimeMillis());
        result.put("status", cuilList.length > 0 ? 200 : 404);
        result.put("message", cuilList.length > 0 ? "Data found" : "Empty response");
        result.put("data", cuilList);

        return result;

    }

}
