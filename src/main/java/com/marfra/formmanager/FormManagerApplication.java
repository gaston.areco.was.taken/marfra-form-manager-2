package com.marfra.formmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
		"com.marfra"
})
public class FormManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormManagerApplication.class, args);
	}

}
